package com.taf.testscenarios.ui;

import com.taf.TAF;
import com.taf.testscenarios.CommonBaseTest;
import org.testng.annotations.*;

import java.lang.reflect.Method;

import static com.taf.configuration.extentreports.ExtentReportsManager.includeTestToReport;

public class BaseUITest extends CommonBaseTest {
    protected TAF taf = new TAF(true, null);

    @BeforeSuite(alwaysRun = true)
    @Parameters("browserSize")
    public void resizingToMobileView(@Optional String browserSize) {
        if (browserSize != null && browserSize.equals("MobileView")) {
            taf.switchToMobileView();
        }
    }

    @BeforeMethod(alwaysRun = true)
    public void reporting(Method method) {
        includeTestToReport(method.getAnnotation(Test.class));
    }
}
