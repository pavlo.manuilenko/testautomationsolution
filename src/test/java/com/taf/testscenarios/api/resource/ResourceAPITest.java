package com.taf.testscenarios.api.resource;

import com.taf.layers.api.models.resource.Resource;
import com.taf.layers.api.models.resource.ResourceData;
import com.taf.testscenarios.api.BaseAPITest;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static com.taf.constants.APIConst.*;

public class ResourceAPITest extends BaseAPITest {

    @Test(suiteName = "ResourceAPITest",
            testName = "Verify id, name and year from single resource info",
            description = "Requirements: Task_api-07",
            groups = "Regression")
    public void idYearNameSingleResourceTest() {
        ResourceData resourceData = taf.getRestClient()
                .sendGetRequest(BASE_API_URL + RESOURCE_ENDPOINT + "1", CONTENT_TYPE_JSON)
                .getResponseAsObject("data", ResourceData.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(resourceData.getName()).isEqualTo("cerulean");
        Assertions.assertThat(resourceData.getYear()).isEqualTo("2000");
    }


    @Test(suiteName = "ResourceAPITest",
            testName = "Verify all resources count and total count resources are match",
            description = "Requirements: Task_api-08")
    public void totalCountResourceTest() {
        Set<Integer> resourcesID = new TreeSet<>();
        Resource resource = taf.getRestClient()
                .sendGetRequest(BASE_API_URL + RESOURCE_ENDPOINT, CONTENT_TYPE_JSON)
                .getResponseAsObject(".", Resource.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        for (int i = 1; i <= resource.getTotal_pages(); i++) {
            resourcesID.addAll(taf.getRestClient()
                    .setRequestParam("page", "" + i)
                    .sendGetRequest(BASE_API_URL + RESOURCE_ENDPOINT, CONTENT_TYPE_JSON)
                    .getResponseAsObjectsList("data", ResourceData.class)
                    .stream().map(ResourceData::getId).collect(Collectors.toList()));
        }
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(resourcesID).hasSize(resource.getTotal());
    }

    @Test(suiteName = "ResourceAPITest",
            testName = "Verify resources are not present after the last page",
            description = "Requirements: Task_api-08")
    public void noResourcesPresentOnThePageAfterTheLastOneTest() {
        int totalPages = taf.getRestClient()
                .sendGetRequest(BASE_API_URL + RESOURCES_ENDPOINT, CONTENT_TYPE_JSON)
                .getResponseAsObject(".", Resource.class).getTotal_pages();
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        taf.getRestClient().setRequestParam("page", totalPages + 1 + "")
                .sendGetRequest(BASE_API_URL + RESOURCES_ENDPOINT, CONTENT_TYPE_JSON);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(taf.getRestClient().getResponseAsObjectsList("data", ResourceData.class)).isEmpty();
    }
}
