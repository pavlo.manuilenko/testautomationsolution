package com.taf.testscenarios.api;

import com.taf.TAF;
import com.taf.layers.api.ServerInterceptorCalls;
import com.taf.testscenarios.CommonBaseTest;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static com.taf.configuration.extentreports.ExtentReportsManager.includeTestToReport;

public class MockedBaseAPITest extends CommonBaseTest {
    protected TAF taf = new TAF(false, ServerInterceptorCalls.getInstance());

    @BeforeSuite(alwaysRun = true)
    public void initInterceptor() {
        taf.getInterceptor().start();
    }

    @BeforeMethod(alwaysRun = true)
    public void reporting(Method method) {
        includeTestToReport(method.getAnnotation(Test.class));
    }

    @AfterSuite(alwaysRun = true)
    public void shutDownInterceptor() {
        taf.getInterceptor().stop();
    }
}
