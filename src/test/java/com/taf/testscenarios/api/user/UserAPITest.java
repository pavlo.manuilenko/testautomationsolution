package com.taf.testscenarios.api.user;

import com.taf.layers.api.models.user.LoginRegUserReq;
import com.taf.layers.api.models.user.LoginRegUserResp;
import com.taf.layers.api.models.user.UserData;
import com.taf.testscenarios.api.BaseAPITest;
import com.taf.utils.helpers.APIHelper;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.taf.constants.APIConst.*;

public class UserAPITest extends BaseAPITest {

    @Test(suiteName = "UserAPITest",
            testName = "Verify id, first_name and last_name from single user info",
            description = "Requirements: Task_api-01")
    public void idAndFullNameSingleUserTest() {
        UserData userData = taf.getRestClient()
                .sendGetRequest(BASE_API_URL + USER_ENDPOINT + "1", CONTENT_TYPE_JSON)
                .getResponseAsObject("data", UserData.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(userData.getFirst_name()).isEqualTo("George");
        Assertions.assertThat(userData.getLast_name()).isEqualTo("Bluth");
    }

    @Test(suiteName = "UserAPITest",
            testName = "Verify id and id in avatar link are match from single user info",
            description = "Requirements: Task_api-01")
    public void idAndIdInAvatarLinkAreMatchForSingleUserTest() {
        UserData userData = taf.getRestClient()
                .sendGetRequest(BASE_API_URL + USER_ENDPOINT + "1", CONTENT_TYPE_JSON)
                .getResponseAsObject("data", UserData.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(APIHelper.getIdFromUserAvatarLink(userData.getAvatar()))
                .isEqualTo(userData.getId().toString());
    }

    @Test(suiteName = "UserAPITest",
            testName = "Get single user info - not found test (negative)",
            description = "Requirements: Task_api-01")
    public void singleUserNotFoundTest() {
        taf.getRestClient().sendGetRequest(BASE_API_URL + USER_ENDPOINT + "23", CONTENT_TYPE_JSON);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(404);
    }

    @Test(suiteName = "UserAPITest",
            testName = "User successful register",
            description = "Requirements: Task_api-02",
            groups = "Regression")
    public void userSuccessfulRegister() {
        LoginRegUserResp regUser = taf.getRestClient()
                .sendPostRequest(BASE_API_URL + USER_REG_ENDPOINT, CONTENT_TYPE_JSON, new LoginRegUserReq("eve.holt@reqres.in", "pistol"))
                .getResponseAsObject(".", LoginRegUserResp.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(regUser.getId()).isEqualTo(4);
        Assertions.assertThat(regUser.getToken()).isEqualTo("QpwL5tke4Pnpja7X4");
    }

    @Test(suiteName = "UserAPITest",
            testName = "User unsuccessful register",
            description = "Requirements: Task_api-02")
    public void userUnsuccessfulRegister() {
        Assertions.assertThat(taf.getRestClient()
                .sendPostRequest(BASE_API_URL + USER_REG_ENDPOINT, CONTENT_TYPE_JSON, new LoginRegUserReq("eve.holt@reqres.in"))
                .getStatusCode()).isEqualTo(400);
        Assertions.assertThat(taf.getRestClient().getResponseAsObject(".", LoginRegUserResp.class).getError())
                .isEqualTo("Missing password");
    }

    @Test(suiteName = "UserAPITest",
            testName = "Verify update date when user name and job updated",
            description = "Requirements: Task_api-03",
            dependsOnMethods = {"idAndFullNameSingleUserTest"}
    )
    public void updateUserNameAddJobTest() {
        String name = "Adam";
        String job = "QA engineer";
        String shouldBeUpdatedAt = java.time.Clock.systemUTC().instant().toString();
        UserData userData = taf.getRestClient()
                .sendPutRequest(BASE_API_URL + USER_ENDPOINT + "1", CONTENT_TYPE_JSON, new UserData(name, job, null))
                .getResponseAsObject(".", UserData.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(userData.getFirst_name()).isEqualTo(name);
        Assertions.assertThat(userData.getJob()).isEqualTo(job);
        Assertions.assertThat(userData.getUpdatedAt().substring(0, userData.getUpdatedAt().indexOf("T")))
                .isEqualTo(shouldBeUpdatedAt.substring(0, shouldBeUpdatedAt.indexOf("T")));
    }

    @Test(suiteName = "UserAPITest",
            testName = "Verify update date when user name and email updated",
            description = "Requirements: Task_api-03",
            dependsOnMethods = {"idAndFullNameSingleUserTest"})
    public void updateUserNameAddEmailTest() {
        String name = "Adam";
        String email = "new_adam@google.com";
        String shouldBeUpdatedAt = java.time.Clock.systemUTC().instant().toString();
        UserData userData = taf.getRestClient()
                .sendPatchRequest(BASE_API_URL + USER_ENDPOINT + "1", CONTENT_TYPE_JSON, new UserData(name, null, email))
                .getResponseAsObject(".", UserData.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(userData.getFirst_name()).isEqualTo(name);
        Assertions.assertThat(userData.getEmail()).isEqualTo(email);
        Assertions.assertThat(userData.getUpdatedAt().substring(0, userData.getUpdatedAt().indexOf("T")))
                .isEqualTo(shouldBeUpdatedAt.substring(0, shouldBeUpdatedAt.indexOf("T")));
    }

    @Test(suiteName = "UserAPITest",
            testName = "Verify id are sorted in list users info",
            description = "Requirements: Task_api-04")
    public void idAreSortedInUsersListTest() {
        List<UserData> usersData = taf.getRestClient()
                .sendGetRequest(BASE_API_URL + ALL_USERS_ENDPOINT, CONTENT_TYPE_JSON)
                .getResponseAsObjectsList("data", UserData.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThatCollection(usersData.stream()
                        .sorted(Comparator.comparing(UserData::getId)).toList()
                        .stream().map(UserData::getId).collect(Collectors.toList()))
                .containsExactlyElementsOf(usersData.stream().map(UserData::getId).collect(Collectors.toList()));
    }

    @Test(suiteName = "UserAPITest",
            testName = "Verify id and id in avatar link are match from users info",
            description = "Requirements: Task_api-04")
    public void idAndIdInAvatarLinkAreMatchForUsersTest() {
        List<UserData> usersData = taf.getRestClient()
                .sendGetRequest(BASE_API_URL + ALL_USERS_ENDPOINT, CONTENT_TYPE_JSON)
                .getResponseAsObjectsList("data", UserData.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        for (UserData singleUserData : usersData) {
            Assertions.assertThat(APIHelper.getIdFromUserAvatarLink(singleUserData.getAvatar()))
                    .isEqualTo(singleUserData.getId().toString());
        }
    }

    @Test(suiteName = "UserAPITest",
            testName = "Verify User delete via id is successful",
            description = "Requirements: Task_api-05")
    public void deleteSingleUserTest() {
        taf.getRestClient()
                .sendDeleteRequest(BASE_API_URL + USER_ENDPOINT + "1");
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(204);
    }


    @Test(suiteName = "UserAPITest",
            testName = "User successful log in",
            description = "Requirements: Task_api-06",
            groups = "Regression")
    public void userLogInSuccessful() {
        LoginRegUserResp regUser = taf.getRestClient()
                .sendPostRequest(BASE_API_URL + USER_LOGIN_ENDPOINT, CONTENT_TYPE_JSON, new LoginRegUserReq("eve.holt@reqres.in", "pistol"))
                .getResponseAsObject(".", LoginRegUserResp.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(regUser.getToken()).isEqualTo("QpwL5tke4Pnpja7X4");
    }
}
