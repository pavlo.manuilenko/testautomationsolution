/**
 * Utility Config class should not have public constructors
 * Using for getting Constants throughout the TAF
 */
package com.taf.constants;

import com.taf.enums.Browser;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.taf.configuration.TAFConfig.*;
import static com.taf.constants.TAFConst.TAF_ENV_PROPERTIES;
import static com.taf.constants.TAFConst.TAF_MAIN_PROPERTIES;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UIConst {
    public static final boolean HEADLESS = getSystemProperty("headless").equalsIgnoreCase("true");
    public static final Browser BROWSER = Browser.valueOfName(getSystemProperty("ui.browser"));
    public static final String DESKTOP_BROWSER_SIZE = getProperty(TAF_MAIN_PROPERTIES, "ui.desktopBrowserSize");
    public static final String MOBILE_BROWSER_SIZE = getProperty(TAF_MAIN_PROPERTIES, "ui.mobileBrowserSize");
    public static final long BROWSER_TIMEOUT = Long.parseLong(getProperty(TAF_MAIN_PROPERTIES, "ui.browserTimeout"));
    public static final String BASE_UI_URL = getProperty(TAF_ENV_PROPERTIES, "ui.baseUrl");
    public static final String ABOUT_GOOGLE_PAGE_UI_URL = getProperty(TAF_ENV_PROPERTIES, "ui.aboutGooglePageUrl");
    public static final String GUI_TEXTS_DATA = getProperty(TAF_MAIN_PROPERTIES, "ui.texts.data");
}
