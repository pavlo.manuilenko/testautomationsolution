/**
 * Utility Helper class should not have public constructors
 * Used to transform some API responses data to test data
 */
package com.taf.utils.helpers;

public class APIHelper {
    public static String getIdFromUserAvatarLink(String avatarLink) {
        return avatarLink.substring(avatarLink.lastIndexOf("/") + 1,
                avatarLink.lastIndexOf("-"));
    }

    public static String getIdFromRetailerLicense(String retailerLicense) {
        return retailerLicense.substring(retailerLicense.indexOf("-") + 1,
                retailerLicense.lastIndexOf("-"));
    }
}
