/**
 * Utility Config class should not have public constructors
 * Using for get Properties from create required browser instance, managed by Selenide
 */
package com.taf.configuration;

import com.codeborne.selenide.Browsers;
import com.codeborne.selenide.Configuration;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.chrome.ChromeOptions;

import static com.taf.constants.UIConst.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Log4j2
public class DriverManager {
    private static boolean browserInit = false;

    public static void initBrowser() {
        switch (BROWSER) {
            case SAFARI:
                Configuration.browser = Browsers.SAFARI;
                break;
            case EDGE:
                Configuration.browser = Browsers.EDGE;
                break;
            case FIREFOX:
                Configuration.browser = Browsers.FIREFOX;
                break;
            default:
                Configuration.browser = Browsers.CHROME;
                Configuration.browserCapabilities = new ChromeOptions().addArguments("--lang=en_US");
        }
        browserInit = true;
        Configuration.timeout = BROWSER_TIMEOUT;
        Configuration.browserSize = DESKTOP_BROWSER_SIZE;
        Configuration.headless = HEADLESS;
        Configuration.holdBrowserOpen = false;
    }

    public static void setBrowserSize(String size) {
        Configuration.browserSize = size;
        log.info("Browser size is: " + Configuration.browserSize);
    }

    public static String getBrowserSize() {
        return Configuration.browserSize;
    }

    public static boolean wasBrowserInitiated() {
        return browserInit;
    }
}
