/**
 * Utility Config class should not have public constructors
 * extentTestMap holds the information of thread ids and ExtentTest instances.
 * At startTest() method, an instance of ExtentTest created and put into extentTestMap with current thread id.
 * At getTest() method, return ExtentTest instance in extentTestMap by using current thread id.
 */
package com.taf.configuration.extentreports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static com.taf.configuration.DriverManager.getBrowserSize;
import static com.taf.configuration.DriverManager.wasBrowserInitiated;
import static com.taf.configuration.TAFConfig.getSystemProperty;
import static com.taf.constants.TAFConst.*;
import static com.taf.constants.UIConst.BROWSER;
import static com.taf.constants.UIConst.HEADLESS;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExtentReportsManager {
    public static final ExtentReports extentReports = createExtentReports();
    static Map<Integer, ExtentTest> extentTestMap = new HashMap<>();

    @SneakyThrows
    private static synchronized ExtentReports createExtentReports() {
        ExtentReports extReports = new ExtentReports();
        ExtentSparkReporter reporter = new ExtentSparkReporter(REPORT_OUTPUT_LOCATION);
        reporter.loadXMLConfig("src/main/resources/reporting/extent-config.xml");
        extReports.attachReporter(reporter);
        extReports.setSystemInfo("Project", PROJECT_NAME);
        extReports.setSystemInfo("System under test", SYSTEM_UNDER_TEST);
        extReports.setSystemInfo("Run in thread(s)", getSystemProperty("threadCount", "1"));
        extReports.setSystemInfo("TestNG Suite Name", String.format("%s.xml", getSystemProperty("testSuiteName", "all_tests")));
        if (wasBrowserInitiated()) {
            extReports.setSystemInfo("Browser", String.valueOf(BROWSER));
            extReports.setSystemInfo("Resolution", getBrowserSize());
            extReports.setSystemInfo("Headless mode", String.valueOf(HEADLESS));
        }
        return extReports;
    }

    public static synchronized ExtentTest getTest() {
        return extentTestMap.get((int) Thread.currentThread().getId());
    }

    public static synchronized void includeTestToReport(@NonNull Test testAnnotation) {
        ExtentTest test;
        if (testAnnotation.testName() == null || testAnnotation.testName().isEmpty()) {
            test = extentReports.createTest("[Absent name]", testAnnotation.description());
            log.warn("Test Name is absent!");
        } else {
            test = extentReports.createTest(testAnnotation.testName(), testAnnotation.description());
        }
        test.assignCategory(testAnnotation.suiteName().isEmpty() ? "Other" : testAnnotation.suiteName());
        extentTestMap.put((int) Thread.currentThread().getId(), test);
    }

    public static synchronized void showMethodInvocationInReportDetails(boolean showAsStep) {
        final StackTraceElement traceElement = Thread.currentThread().getStackTrace()[2];
        String className = traceElement.getClassName();
        String methodName = traceElement.getMethodName();
        if (showAsStep) {
            className = String.format("(%s)", className.substring(className.lastIndexOf('.') + 1));
            methodName = String.format("<b> %s</b>", StringUtils.capitalize(StringUtils.join(StringUtils.
                    splitByCharacterTypeCamelCase(methodName), ' ').toLowerCase()));
        } else {
            className = className.substring(className.lastIndexOf('.') + 1);
            methodName = String.format(".<b>%s</b>();", methodName);
        }
        getTest().log(Status.INFO, className + methodName);
    }
}
