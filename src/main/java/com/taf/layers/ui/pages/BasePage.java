package com.taf.layers.ui.pages;

import com.codeborne.selenide.WebElementCondition;
import lombok.AccessLevel;
import lombok.Getter;
import org.openqa.selenium.support.ui.ExpectedCondition;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

public abstract class BasePage {
    protected static final long PAGE_LOAD_TIMEOUT_IN_SECONDS = 5;
    @Getter(AccessLevel.PROTECTED)
    private WebElementCondition clickable = and("can be clicked", visible, enabled);

    private boolean waitForJScriptLoad() {
        final String jsReadyScript = "return document.readyState";
        ExpectedCondition<Boolean> jsLoad = webDriver -> executeJavaScript(jsReadyScript).toString().equals("complete");
        return Wait().until(jsLoad);
    }

    public boolean waitUntilPageIsLoaded() {
        return waitForJScriptLoad();
    }

    public String getCurrentUrl() {
        return url();
    }

    public void cleansBrowserCookies() {
        clearBrowserCookies();
    }
}
