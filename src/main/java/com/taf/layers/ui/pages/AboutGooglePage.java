package com.taf.layers.ui.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.taf.utils.helpers.FileHelper;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;

public class AboutGooglePage extends BasePage {
    public static final String EXP_PAGE_URL = "https://about.google/";
    public static final String EXP_TITLE_TEXT = FileHelper.getStringFromFile("about_google_page_title_en_text");
    public static final String EXP_HEADER_TEXT = FileHelper.getStringFromFile("about_google_page_header_en_text");
    private static final String HEADER_LOCATOR = "//h1";

    public AboutGooglePage() {
        waitUntilPageIsLoaded();
    }

    public AboutGooglePage(String aboutGooglePageAddress) {
        open(aboutGooglePageAddress);
        waitUntilPageIsLoaded();
    }

    public SelenideElement getTitle() {
        return $(By.xpath("//title[text()]"));
    }

    @Override
    public boolean waitUntilPageIsLoaded() {
        super.waitUntilPageIsLoaded();
        return $(By.xpath(HEADER_LOCATOR)).should(Condition.visible, Duration.ofSeconds(PAGE_LOAD_TIMEOUT_IN_SECONDS)).isDisplayed();
    }

    public SelenideElement getHeaderFromPage() {
        return $(By.xpath(HEADER_LOCATOR));
    }

    @Override
    public String getCurrentUrl() {
        return url().substring(0, url().indexOf('?'));
    }
}
