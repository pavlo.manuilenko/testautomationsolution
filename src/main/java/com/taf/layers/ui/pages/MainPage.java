package com.taf.layers.ui.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.taf.layers.ui.components.Footer;
import lombok.Getter;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.taf.configuration.extentreports.ExtentReportsManager.showMethodInvocationInReportDetails;

public class MainPage extends BasePage {
    @Getter
    private SelenideElement title = $(By.xpath("//title[text()]"));
    private static final String GOOGLE_LOGO = "//img[@alt='Google']";
    private static final String SEARCH_FIELD = "//*[@name='q']";
    @Getter
    private Footer footer = new Footer(); //Composition of footer aria element

    public MainPage(String address) {
        open(address);
    }

    public boolean isGoogleLogoPresent() {
        return $(By.xpath(GOOGLE_LOGO)).shouldBe(visible, enabled).isDisplayed();
    }

    public boolean isGoogleSearchFieldPresent() {
        return $(By.xpath(SEARCH_FIELD)).shouldBe(getClickable()).isDisplayed();
    }

    public AboutGooglePage openAboutGooglePageFromFooter() {
        showMethodInvocationInReportDetails(true);
        return footer.openAboutGooglePage();
    }

    @Override
    public boolean waitUntilPageIsLoaded() {
        super.waitUntilPageIsLoaded();
        return $(By.xpath(GOOGLE_LOGO)).should(Condition.visible, Duration.ofSeconds(PAGE_LOAD_TIMEOUT_IN_SECONDS)).isDisplayed();
    }
}
