package com.taf.layers.ui.components;

import com.taf.layers.ui.pages.AboutGooglePage;
import com.taf.utils.helpers.FileHelper;
import lombok.Getter;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$$;

public class Footer {
    @Getter
    private final List<String> footerItems = FileHelper.getStringListFromFile("footer_items_en_texts");
    private static final String FOOTER_ELEMENTS_LOCATOR = "//a[@class='pHiOh']";

    public int getFooterElementsCount() {
        return $$(By.xpath(FOOTER_ELEMENTS_LOCATOR)).size();
    }

    public List<String> getFooterElementsNames() {
        return $$(By.xpath(FOOTER_ELEMENTS_LOCATOR)).texts().stream().map(String::trim).collect(Collectors.toList());

    }

    public AboutGooglePage openAboutGooglePage() {
        $$(By.xpath(FOOTER_ELEMENTS_LOCATOR)).get(0).click();
        return new AboutGooglePage();
    }
}
