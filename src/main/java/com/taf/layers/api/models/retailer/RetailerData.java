package com.taf.layers.api.models.retailer;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RetailerData {
    @Getter
    private Integer id;
    @Getter
    private String name;
    @Getter
    private String location;
    @Getter
    private String license;

}
