package com.taf.layers.api.models.user;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserData {
    @Getter
    private Integer id;
    @Getter
    private String email;
    @Getter
    private String first_name;
    @Getter
    private String last_name;
    @Getter
    private String avatar;
    @Getter
    private String job;
    @Getter
    private String updatedAt;

    public UserData(String firstName, String job, String email) {
        this.first_name = firstName;
        this.job = job;
        this.email = email;
    }
}
