package com.taf.layers.api.models.retailer;

import lombok.Getter;

public class RetailerShop {
    @Getter
    private Integer id;
    @Getter
    private String name;
    @Getter
    private String address;

}
