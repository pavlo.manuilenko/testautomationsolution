package com.taf.layers.api.models.resource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Resource {
    @Getter
    private Integer page;
    @Getter
    private Integer per_page;
    @Getter
    private Integer total;
    @Getter
    private Integer total_pages;

}
