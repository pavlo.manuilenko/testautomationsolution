package com.taf.enums;

public enum Browser {
    CHROME("chrome", "Google Chrome"),
    SAFARI("safari", "Apple Safari"),
    EDGE("edge", "MS Edge"),
    FIREFOX("firefox", "Firefox");

    private final String description;
    private final String name;

    Browser(String browsersName, String description) {
        this.description = description;
        this.name = browsersName;
    }

    @Override
    public String toString() {
        return description;
    }

    public String getName() {
        return name;
    }

    public static Browser valueOfName(String browserName) {
        for (Browser browser : values()) {
            if (browser.name.equals(browserName)) {
                return browser;
            }
        }
        return Browser.CHROME;
    }
}
