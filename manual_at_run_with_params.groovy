//Manual run AT declarative pipeline with flexible variables
pipeline {
    agent any

    tools {
        maven 'Maven'
    }

    parameters {
        choice(name: 'TEST_SUITE', choices: ['all_tests', 'all_api_tests', 'all_ui_tests', 'regression_tests', 'mobile_view_tests', 'cross_browser_tests'], description: 'TestNG suite name')
        choice(name: 'ENV', choices: ['ci', 'local'], description: 'Passing properties from a file')
        choice(name: 'THREAD_COUNT', choices: ['1', '3', '10'], description: 'Parallel testing mode (threads)')
        choice(name: 'BROWSER', choices: ['chrome', 'safari', 'edge', 'firefox'], description: 'Choosing a browser for UI testing')
        booleanParam(name: 'HEADLESS', defaultValue: true, description: 'Enabling/disabling headless browser mode')
    }

    stages {
        stage('Repo pulling') {
            steps {
                git 'https://gitlab.com/pavlo.manuilenko/testautomationsolution.git'
            }
        }

        stage('Build and executing tests') {
            steps {
                bat "mvn clean test -DtestSuiteName=${params.TEST_SUITE} -Dheadless=${params.HEADLESS} -Denv=${params.ENV} -DthreadCount=${params.THREAD_COUNT} -Dui.browser=${params.BROWSER}"
            }
        }

        stage('Report publishing') {
            steps {
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: 'target/extent-reports', reportFiles: 'Report.html', reportName: 'AT Report', reportTitles: 'AT Report', useWrapperFileDirectly: true])
            }
        }

    }
}