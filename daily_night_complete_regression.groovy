//Daily night complete regression declarative pipeline
pipeline {
    agent any

    triggers {
        cron('H H(0-1) * * 1-5')
    }

    tools {
        maven 'Maven'
    }

    stages {
        stage('Repo pulling') {
            steps {
                git 'https://gitlab.com/pavlo.manuilenko/testautomationsolution.git'
            }
        }

        stage('Build and executing tests') {
            steps {
                bat 'mvn clean test -DtestSuiteName=all_tests -Dheadless=true -Denv=ci -DthreadCount=3 -Dui.browser=chrome'
            }
        }

        stage('Report publishing') {
            steps {
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: 'target/extent-reports', reportFiles: 'Report.html', reportName: 'AT Report', reportTitles: 'AT Report', useWrapperFileDirectly: true])
            }
        }

    }
}